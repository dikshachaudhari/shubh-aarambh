
$(window).on('load', function() {
    $("#preloader").delay(9300).fadeOut('slow');
    
    /*Slider-image-section*/
    
    $(".owl-carousel").owlCarousel({
        items:1,
        loop:true,
        nav:true,
        dots:true,
        autoplay:true,
        autoplaySpeed:1000,
        smartSpeed:1500,
        autoplayHoverPause:true
        
    });
});